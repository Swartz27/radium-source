#pragma once

namespace GameObject
{
    enum ECallbackType
    {
        eTradeStart = u32(0),
        eTradeStop,
        eTradeSellBuyItem,
        eTradePerformTradeOperation,

        eZoneEnter,
        eZoneExit,
        eExitLevelBorder,
        eEnterLevelBorder,
        eDeath,

        ePatrolPathInPoint,

        eInventoryPda,
        eInventoryInfo,
        eArticleInfo,
        eTaskStateChange,
        eMapLocationAdded,

        eUseObject,

        eHit,

        eSound,

        eActionTypeMovement,
        eActionTypeWatch,
        eActionTypeRemoved,
        eActionTypeAnimation,
        eActionTypeSound,
        eActionTypeParticle,
        eActionTypeObject,

        eActorSleep,

        eHelicopterOnPoint,
        eHelicopterOnHit,

        eOnItemTake,
        eOnItemDrop,

        eScriptAnimation,

        eTraderGlobalAnimationRequest,
        eTraderHeadAnimationRequest,
        eTraderSoundEnd,

        eInvBoxItemTake,
        eWeaponNoAmmoAvailable,

        //AVO: custom callbacks
		// input
        eKeyPress,
        eKeyRelease,
        eKeyHold,
        eMouseMove,
        eMouseWheel,
        // inventory
        eItemToBelt,
        eItemToSlot,
        eItemToRuck,
        eBelt_by_id,        
        /* avo: end */
        eSwitchTorch,
        
//        eWalkAccel,
//        eJumpSpeed,
///        eRunFactor,
//        eRunBackFactor,
 //       eWalkBackFactor,
 //       eCrouchFactor,
 //       eClimbFactor,
  //      eSprintFactor,
		/*************************************************** added by Ray Twitty (aka Shadows) END ***************************************************/

		// Added by Cribbledirge Start.
		/* Added in some mouse press detection for the player character. */
		//eOnMousePress,
		//eOnMouseRelease,
		//eOnMouseHold,

		// These specifically say actor as I intend to add callbacks for NPCs firing their weapons.
		eOnActorWeaponFireEnd,		
		eOnActorWeaponFire,
		eOnActorWeaponJammed,
		eOnActorWeaponEmpty,
		eOnActorWeaponReload,    

		// NPC Weapon Callbacks.
		eOnNPCWeaponFire,
		eOnNPCWeaponFireEnd,
		eOnNPCWeaponJammed,
		eOnNPCWeaponEmpty,
		eOnNPCWeaponReload,    
        eActorBeforeDeath,
		eOnWeaponZoomIn,
		eOnWeaponZoomOut,
		//eOnWeaponZoomIn,
		//eOnWeaponZoomOut,
		//eOnWeaponJammed,

		//eCellItemFocus,
		//eCellItemFocusLost,

		// Cribbledirge End
        eDummy = u32(-1),
    };
};
