#pragma once
#include "customzone.h"
#include "script_export_space.h"

class CTimeAnom : public CCustomZone
{
private:
	typedef	CCustomZone	inherited;
public:
	CTimeAnom(void);
	virtual ~CTimeAnom(void);

	virtual void Load					(LPCSTR section);
	virtual void Affect					(SZoneObjectInfo* O);
	//virtual	void feel_touch_new			(CObject* O	);
	//virtual	void UpdateWorkload			(u32	dt	);				// related to fast-mode optimizations
	//virtual	bool feel_touch_contact		(CObject* O	);
			//float nearest_shape_radius	(SZoneObjectInfo* O);
//private:
	//void RustyItems();
	//float m_rust_damage;
protected:
	virtual bool BlowoutState			();
	u32				m_dwTeleTime;
  float m_time_factor;  
	DECLARE_SCRIPT_REGISTER_FUNCTION
};
add_to_type_list(CTimeAnom)
#undef script_type_list
#define script_type_list save_type_list(CTimeAnom)
