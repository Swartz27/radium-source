#pragma once
#include "customzone.h"
#include "script_export_space.h"

class CRustyFog : public CCustomZone
{
private:
	typedef	CCustomZone	inherited;
public:
	CRustyFog(void);
	virtual ~CRustyFog(void);

	virtual void Load					(LPCSTR section);
	virtual void Affect					(SZoneObjectInfo* O);
	//virtual	void feel_touch_new			(CObject* O	);
	//virtual	void UpdateWorkload			(u32	dt	);				// related to fast-mode optimizations
	//virtual	bool feel_touch_contact		(CObject* O	);
			//float nearest_shape_radius	(SZoneObjectInfo* O);
//private:
	//void RustyItems();
	//float m_rust_damage;
protected:
	virtual bool BlowoutState			();
	u32				m_dwTeleTime;  
	DECLARE_SCRIPT_REGISTER_FUNCTION
};
add_to_type_list(CRustyFog)
#undef script_type_list
#define script_type_list save_type_list(CRustyFog)
