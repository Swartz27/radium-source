#pragma once

#include "../BaseMonster/base_monster.h"
#include "../controlled_entity.h"
#include "../../../../xrServerEntities/script_export_space.h"

class CAI_Bear : public CBaseMonster,
				 public CControlledEntity<CAI_Bear> {

	typedef		CBaseMonster	inherited;
	typedef		CControlledEntity<CAI_Bear>	CControlled;

public:
					CAI_Bear			();
	virtual			~CAI_Bear			();	

	virtual void	Load				(LPCSTR section);
	virtual BOOL	net_Spawn			(CSE_Abstract* DC);
	virtual void	reinit				();

	virtual void	UpdateCL			();

	virtual bool	CanExecRotationJump	() {return true;}
	virtual void	CheckSpecParams		(u32 spec_params);

	// look at enemy
	static void	_BCL	BoneCallback	(CBoneInstance *B);
	
			float	_velocity;
			float	_cur_delta, _target_delta;
			bool	look_at_enemy;
	
	virtual bool	ability_can_drag	() {return true;}

	virtual	char*	get_monster_class_name () { return "bear"; }
	
	DECLARE_SCRIPT_REGISTER_FUNCTION

};

add_to_type_list(CAI_Bear)
#undef script_type_list
#define script_type_list save_type_list(CAI_Bear)