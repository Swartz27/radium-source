#include "stdafx.h"
#include "pch_script.h"
#include "karlik.h"

using namespace luabind;

#pragma optimize("s",on)
void CAI_Karlik::script_register(lua_State *L)
{
	module(L)
	[
		class_<CAI_Karlik,CGameObject>("CAI_Karlik")
			.def(constructor<>())
	];
}
