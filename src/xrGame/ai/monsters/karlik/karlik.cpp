#include "stdafx.h"
#include "karlik.h"
#include "karlik_state_manager.h"
#include "../../../../Include/xrRender/KinematicsAnimated.h"
#include "../monster_velocity_space.h"
#include "../../../game_object_space.h"
#include "../control_animation_base.h"
#include "../control_movement_base.h"


CAI_Karlik::CAI_Karlik()
{
	StateMan = xr_new<CStateManagerKarlik>(this);
	CControlled::init_external(this);  
}

CAI_Karlik::~CAI_Karlik()
{
	xr_delete(StateMan);
}

void CAI_Karlik::Load(LPCSTR section)
{
	inherited::Load	(section);

	anim().AddReplacedAnim(&m_bDamaged, eAnimRun,		eAnimRunDamaged);
	anim().AddReplacedAnim(&m_bDamaged, eAnimWalkFwd,	eAnimWalkDamaged);

	anim().accel_load			(section);
	anim().accel_chain_add		(eAnimWalkFwd,		eAnimRun);
	anim().accel_chain_add		(eAnimWalkDamaged,	eAnimRunDamaged);

	SVelocityParam &velocity_none		= move().get_velocity(MonsterMovement::eVelocityParameterIdle);	
	SVelocityParam &velocity_turn		= move().get_velocity(MonsterMovement::eVelocityParameterStand);
	SVelocityParam &velocity_walk		= move().get_velocity(MonsterMovement::eVelocityParameterWalkNormal);
	SVelocityParam &velocity_run		= move().get_velocity(MonsterMovement::eVelocityParameterRunNormal);
	SVelocityParam &velocity_walk_dmg	= move().get_velocity(MonsterMovement::eVelocityParameterWalkDamaged);
	SVelocityParam &velocity_run_dmg	= move().get_velocity(MonsterMovement::eVelocityParameterRunDamaged);
	SVelocityParam &velocity_steal		= move().get_velocity(MonsterMovement::eVelocityParameterSteal);
	//SVelocityParam &velocity_drag		= move().get_velocity(MonsterMovement::eVelocityParameterDrag);


	anim().AddAnim(eAnimStandIdle,		"stand_idle_",			-1, &velocity_none,				PS_STAND, "stand_fx_f", "stand_fx_b", "stand_fx_l", "stand_fx_r");
	anim().AddAnim(eAnimStandDamaged,	"stand_idle_dmg_",		-1, &velocity_none,				PS_STAND, "stand_fx_f", "stand_fx_b", "stand_fx_l", "stand_fx_r");
	anim().AddAnim(eAnimStandTurnLeft,	"stand_turn_ls_",		-1, &velocity_turn,		PS_STAND, "stand_fx_f", "stand_fx_b", "stand_fx_l", "stand_fx_r");
	anim().AddAnim(eAnimStandTurnRight,	"stand_turn_rs_",		-1, &velocity_turn,		PS_STAND, "stand_fx_f", "stand_fx_b", "stand_fx_l", "stand_fx_r");
	anim().AddAnim(eAnimWalkFwd,			"stand_walk_fwd_",		-1, &velocity_walk,	PS_STAND, "stand_fx_f", "stand_fx_b", "stand_fx_l", "stand_fx_r");
	anim().AddAnim(eAnimWalkDamaged,		"stand_walk_fwd_dmg_",	-1, &velocity_walk_dmg,	PS_STAND, "stand_fx_f", "stand_fx_b", "stand_fx_l", "stand_fx_r");
	anim().AddAnim(eAnimRun,				"stand_run_",			-1,	&velocity_run,		PS_STAND, "stand_fx_f", "stand_fx_b", "stand_fx_l", "stand_fx_r");
	anim().AddAnim(eAnimRunDamaged,		"stand_run_dmg_",		-1,	&velocity_run_dmg,	PS_STAND, "stand_fx_f", "stand_fx_b", "stand_fx_l", "stand_fx_r");
	anim().AddAnim(eAnimAttack,			"stand_attack_",		-1, &velocity_turn,		PS_STAND, "stand_fx_f", "stand_fx_b", "stand_fx_l", "stand_fx_r");
	anim().AddAnim(eAnimDie,				"stand_die_",			0,  &velocity_none,				PS_STAND, "stand_fx_f", "stand_fx_b", "stand_fx_l", "stand_fx_r");
	anim().AddAnim(eAnimCheckCorpse,		"stand_check_corpse_",	-1,	&velocity_none,				PS_STAND, "stand_fx_f", "stand_fx_b", "stand_fx_l", "stand_fx_r");
	anim().AddAnim(eAnimSteal,			"stand_crawl_",			-1, &velocity_steal,			PS_STAND, "stand_fx_f", "stand_fx_b", "stand_fx_l", "stand_fx_r");
	anim().AddAnim(eAnimSitIdle,			"sit_idle_",			-1, &velocity_none,				PS_SIT, "stand_fx_f", "stand_fx_b", "stand_fx_l", "stand_fx_r");
	anim().AddAnim(eAnimSitStandUp,		"sit_stand_up_",		-1, &velocity_none,				PS_SIT, "stand_fx_f", "stand_fx_b", "stand_fx_l", "stand_fx_r");
	anim().AddAnim(eAnimStandSitDown,	"stand_sit_down_",		-1, &velocity_none,				PS_STAND, "stand_fx_f", "stand_fx_b", "stand_fx_l", "stand_fx_r");
	anim().AddAnim(eAnimLookAround,		"stand_look_around_",	-1, &velocity_none,				PS_STAND, "stand_fx_f", "stand_fx_b", "stand_fx_l", "stand_fx_r");
	anim().AddAnim(eAnimEat,				"sit_eat_",				-1, &velocity_none,				PS_SIT, "stand_fx_f", "stand_fx_b", "stand_fx_l", "stand_fx_r");

	anim().AddTransition(PS_STAND,		PS_SIT,		eAnimStandSitDown,	false);
	anim().AddTransition(PS_SIT,			PS_STAND,	eAnimSitStandUp,	false);

	anim().LinkAction(ACT_STAND_IDLE,	eAnimStandIdle);
	anim().LinkAction(ACT_SIT_IDLE,		eAnimSitIdle);
	anim().LinkAction(ACT_LIE_IDLE,		eAnimSitIdle);
	anim().LinkAction(ACT_WALK_FWD,		eAnimWalkFwd);
	anim().LinkAction(ACT_WALK_BKWD,		eAnimWalkFwd);
	anim().LinkAction(ACT_RUN,			eAnimRun);
	anim().LinkAction(ACT_EAT,			eAnimEat);
	anim().LinkAction(ACT_SLEEP,			eAnimSitIdle);
	anim().LinkAction(ACT_REST,			eAnimSitIdle);
	anim().LinkAction(ACT_DRAG,			eAnimStandIdle);
	anim().LinkAction(ACT_ATTACK,		eAnimAttack);
	anim().LinkAction(ACT_STEAL,			eAnimSteal);
	anim().LinkAction(ACT_LOOK_AROUND,	eAnimLookAround);

	m_fDeltaPower = pSettings->r_float(section,"MissPowerOnHit");

#ifdef DEBUG	
	anim().accel_chain_test		();
#endif

}

void CAI_Karlik::reinit()
{
	inherited::reinit();
	if(CCustomMonster::use_simplified_visual())	return;
	//com_man().add_rotation_jump_data("stand_jump_left_0",0,"stand_jump_right_0",0, PI - PI_DIV_6, SControlRotationJumpData::eStopAtOnce | SControlRotationJumpData::eRotateOnce);
}


void  CAI_Karlik::BoneCallback(CBoneInstance *B)
{
	CAI_Karlik	*P = static_cast<CAI_Karlik*>(B->callback_param());

	if (!P->look_at_enemy) return;
	
	Fmatrix M;
	M.setHPB (0.0f,-P->_cur_delta,0.0f);
	B->mTransform.mulB_43(M);
}

BOOL CAI_Karlik::net_Spawn (CSE_Abstract* DC) 
{
	if (!inherited::net_Spawn(DC))
		return(FALSE);
	
	if(!PPhysicsShell())//������ ������� �������, ���� ������ ��� ��� - � ���� ����� ���� �������!!!
	{
		CBoneInstance& BI = smart_cast<IKinematics*>(Visual())->LL_GetBoneInstance(smart_cast<IKinematics*>(Visual())->LL_BoneID("bip01_head"));
		BI.set_callback(bctCustom,BoneCallback,this);
	}
	
	_cur_delta		= _target_delta = 0.f;
	_velocity		= PI;
	look_at_enemy	= false;
	return TRUE;
}

void CAI_Karlik::CheckSpecParams(u32 spec_params)
{
	if ((spec_params & ASP_CHECK_CORPSE) == ASP_CHECK_CORPSE) {
		com_man().seq_run(anim().get_motion_id(eAnimCheckCorpse));
	}

	if ((spec_params & ASP_STAND_SCARED) == ASP_STAND_SCARED) {
		anim().SetCurAnim(eAnimLookAround);
		return;
	}
}

void CAI_Karlik::UpdateCL()
{
	inherited::UpdateCL();
	angle_lerp(_cur_delta, _target_delta, _velocity, client_update_fdelta());
}


