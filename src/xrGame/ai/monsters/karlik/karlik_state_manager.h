#pragma once
#include "../monster_state_manager.h"

class CAI_Karlik;

class CStateManagerKarlik : public CMonsterStateManager<CAI_Karlik> {
	typedef CMonsterStateManager<CAI_Karlik> inherited;
public:
					CStateManagerKarlik		(CAI_Karlik *monster); 
	//virtual			~CStateManagerKarlik	();

	virtual void	execute					();
	virtual void	remove_links		(CObject* object) { inherited::remove_links(object);}
//private:
	//float				m_last_health;
};

